(setq user-full-name "MDBDEVIO")

;;Select my preffered theme:
(setq doom-theme 'doom-dracula)
;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

; Set preffered fonts:
(setq doom-font (font-spec :family "JetBrains Mono" :size 15)
      doom-variable-pitch-font (font-spec :family "Ubuntu" :size 15)
      doom-big-font (font-spec :family "JetBrains Mono" :size 24))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

;;Setup Bookmarks
(setq bookmark-default-file "/home/martin/Nextcloud/90-99_Repos/92-PersonalRepos/92.01-Doom/bookmarks")

(map! :leader
      (:prefix ("b". "buffer")
       :desc "List bookmarks"                          "L" #'list-bookmarks
       :desc "Set bookmark"                            "m" #'bookmark-set
       :desc "Delete bookmark"                         "M" #'bookmark-set
       :desc "Save current bookmarks to bookmark file" "w" #'bookmark-save))

(evil-define-key 'normal ibuffer-mode-map
  (kbd "f c") 'ibuffer-filter-by-content
  (kbd "f d") 'ibuffer-filter-by-directory
  (kbd "f f") 'ibuffer-filter-by-filename
  (kbd "f m") 'ibuffer-filter-by-mode
  (kbd "f n") 'ibuffer-filter-by-name
  (kbd "f x") 'ibuffer-filter-disable
  (kbd "g h") 'ibuffer-do-kill-lines
  (kbd "g H") 'ibuffer-update)

;;Global Auto Revert
(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)

;; If you use `org' and don't want your org iles in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/")

(setq org-agenda-files '("/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/inbox.org"
                         "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/org-gtd-tasks.org"
                         "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/gtd_archive_2023"))

;; CUSTOM org TODO states
(after! org
(setq org-todo-keywords
      '((sequence "TODO(t)"
         "NEXT(n)"
         "PLANNING(p)"
         "IN-PROGRESS(i)"
         "WEEKLY-GOAL(m)"
         "GOAL(g)"
         "WAITING(w)"
         "WORK(b)"
         "HABIT(h)"
         "PROJECT(P)"
         "CALENDAR(c)"
         "NOTE(N)"
         "AREA(a)"
         "|"
         "DONE(d!)"
         "COMPLETE(C!)"
         "HOLD(h)"
         "SOMEDAY(s)"
         "RABBITHOLE!(R)")
        )))

;; CUSTOM TODO colors
(after! org
(setq org-todo-keyword-faces
      '(
        ("TODO" . (:foreground "gold" :weight bold))
        ("NEXT" . (:foreground "light coral" :weight bold))
        ("PLANNING" . (:foreground "dark violet" :weight bold))
        ("IN-PROGRESS" . (:foreground "DarkOrange" :weight bold))
        ("WEEKLY-GOAL" . (:foreground "light sea green" :weight bold))
        ("GOAL" . (:foreground "LimeGreen" :weight bold))
        ("WAITING" . (:foreground "LightPink1" :weight bold))
        ("WORK" . (:foreground "Cyan" :weight bold))
        ("HABIT" . (:foreground "RoyalBlue3" :weight bold))
        ("PROJECT" . (:foreground "SlateBlue1" :weight bold))
        ("CALENDAR" . (:foreground "chocolate" :weight bold))
        ("NOTE" . (:foreground "chocolate" :weight bold))
        ("AREA" . (:foreground "LimeGreen" :weight bold))

        ("DONE" . (:foreground "white" :weight bold))
        ("COMPLETE" . (:strikethrough t :foreground "light gray" :weight bold))
        ("HOLD" . (:foreground "Grey46" :weight bold))
        ("SOMEDAY" . (:foreground "cyan1" :weight bold))
        ("RABBITHOLE!" . (:strikethrough t :foreground "red" :weight bold))
        )))

;; Custom Tag colors
(setq org-tag-faces
      '(
        ("planning"  . (:foreground "mediumPurple1" :weight bold))
        ("@research"   . (:foreground "royalblue1"    :weight bold))
        ("QA"        . (:foreground "sienna"        :weight bold))
        ("CRITICAL"  . (:foreground "red1"          :weight bold))
        ("HABIT"  . (:foreground "pink"          :weight bold))
        )
      )

;; Used to open specific commonly used files

(map! :leader
      (:prefix ("=" . "open file")
       :desc "Edit TODO File" "t" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/org-gtd-tasks.org"))
       :desc "Edit Goals File"   "g" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/Goals.org"))
       :desc "Edit INBOX File" "i" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/inbox.org"))))
     ;  :desc "Edit WORK File" "w" #'(lambda () (interactive) (find-file "~/Emacs/Org/WORK_TODO.org"))
    ; :desc "Edit LINKS File" "l" #'(lambda () (interactive) (find-file "~/Emacs/Brain/_Inbox/LINKS.org"))
     ;  :desc "Edit Repeating File" "r" #'(lambda () (interactive) (find-file "~/Emacs/Org/REPEATING.org"))))
(map! :leader
      (:prefix ("= d" . "Open Doom Files")
       :desc "Edit Doom config.el"   "c" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/90-99_Repos/92-PersonalRepos/92.01-Doom/README.org"))
       :desc "Edit Doom init.el"   "i" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/90-99_Repos/92-PersonalRepos/92.01-Doom/init.el"))
       :desc "Edit Doom packages.el"   "p" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/90-99_Repos/92-PersonalRepos/92.01-Doom/packages.el"))))
(map! :leader
      (:prefix ("= b" . "Open Blog Files")
       :desc "Open Blog Root Folder"   "r" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49_Career/44-Blog"))
       :desc "Edit Index.org file"   "i" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49_Career/44-Blog/index.org"))
       :desc "Edit Blog.org file"   "b" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49_Career/44-Blog/Articles/Blog.org"))
       :desc "Edit Emacs.org file"   "e" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49_Career/44-Blog/Emacs.org"))
       :desc "Edit Infosec.org file"   "I" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49_Career/44-Blog/Infosec.org"))))
(map! :leader
      (:prefix ("= p" . "Open areas/Projects")
       :desc "Open Projects Folder" "p" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/00Projects"))
       :desc "Open Systems Folder" "0" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/01-09_System"))
       :desc "Open Health Folder" "1" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/10-19_Health"))
       :desc "Open Home Folder" "2" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/20-29_Home"))
       :desc "Open Relationships Folder" "3" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/30-39_Relationships"))
       :desc "Open Career Folder" "4" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49_Career"))
       :desc "Open Personal Development Folder" "5" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/50-59_PersonalDevelopment"))
       :desc "Open Work Folder" "6" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/60-69_Work"))
       :desc "Open Finances Folder" "7" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/70-79_Finances"))
       :desc "Open Hobbies Folder" "8" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/80-89_Hobbies"))
       :desc "Open Repos Folder" "9" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/90-99_Repos"))))
(map! :leader
      (:prefix ("= h" . "Open Hacking Files")
       :desc "Open Hacking CTF's Directory"   "c" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49-Career/46-Boxes/"))
       ;;:desc "Open Hacking root Folder" "r" #'(lambda () (interactive) (find-file "~/Emacs/Brain/3.Resources/Pentesting/"))
       ;;:desc "Open Hacking Org File" "t" #'(lambda () (interactive) (find-file "~/Emacs/Org/HACKING.org"))
       :desc "Hacking Best tools Doc" "b" #'(lambda () (interactive) (find-file "/home/martin/Nextcloud/40-49-Career/47-Pentesting_Resources/47.01 Best Tools/1.BestTools.org"))))

;;Org capture templates;
(after! org
  (setq org-capture-templates
        '(
;; Add to inbox
          ("i" "INBOX"
        entry (file+headline "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/inbox.org" "INBOX")
         "* TODO %?"
         :empty-lines 0)
;; Add notes to inbox:
        ("n" "Personal Notes/Scatch Pad"
         entry (file+headline "~/Emacs/Org/NOTES.org" "Personal Notes")
         "** %?"
         :empty-lines 0)
;; To create work todos
        ;("w" "Work-Todo"
        ; entry (file+headline "~/Emacs/Org/WORK_TODO.org" "Work-TODO")
        ; "* WORK %?"
        ; :empty-lines 0)
        ("w" "Work-Todo" entry (file "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/inbox.org")
         "* WORK %?"
         :empty-lines 1)
;; To create work notes
        ("W" "Work-Note" entry (file "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/inbox.org")
         "* NOTE %?"
         :empty-lines 0)
;; To create achievments todos
        ("a" "Achievments"
         entry (file+datetree "~/Emacs/Brain/2.Areas/PersonalDevelopment/ACHIEVMENTS.org" "Achievments")
          "* %?"
          :empty-lines 0)
;; Add to Gratitude Diary
        ("g" "Gratidude Diary"
         entry (file+datetree "~/Emacs/Brain/2.Areas/PersonalDevelopment/GRATITUDE.org" "Gratitude Diary")
          "* %?"
          :empty-lines 0)
;; Add to Links Document:
        ("l" "Links" entry (file "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd/inbox.org")
          "* LINK %?"
         :empty-lines 0)
 ;; Weekly Reviews
        ("R" "Weekly Review"
         entry (file+datetree "~/Emacs/Brain/2.Areas/PersonalDevelopment/WeeklyReviews.org" "Weekly Reviews")
         "* %?"
          :empty-lines 0)
       )))

;; $DOOMDIR/config.el
(use-package! org-pandoc-import :after org)

;;;;;;;;;;;;;;;;;;;;;ORG CRYPT
;; ORG CRYPT TAG Setup for inline encryption
;; If I place "crypt" tag in any entry it will encrypt it.
(require 'org-crypt)
(org-crypt-use-before-save-magic)
(setq org-tags-exclude-from-inheritance '("crypt"))
;; GPG key to use for encryption
;; Either the Key ID or set to nil to use symmetric encryption.
(setq org-crypt-key nil)
;; Set shortut to decrypt easier.
(map! :leader
      :desc "Org Decrypt Entry"
      "d e" #'org-decrypt-entry)

;; Org super agend setup:
 (use-package! org-super-agenda
   :after org-agenda
   :init
   (setq org-agenda-skip-scheduled-if-done t
       org-agenda-skip-deadline-if-done t
       org-agenda-include-deadlines t
      ;; org-agenda-block-separator nil
      ;;   org-agenda-compact-blocks t
       org-agenda-start-day nil ;; i.e. today
       org-agenda-span 1
       org-agenda-start-on-weekday nil)
   (setq org-agenda-custom-commands
         '(("c" "Super view"
                      ((agenda "" ((org-agenda-span 'day)
                       (org-super-agenda-groups
                        '((:name "⏰⏰⏰⏰⏰ --- Today --- ⏰⏰⏰⏰⏰"
                           :discard (:todo "DONE")
                           :discard (:tag "habit")
                           :time-grid t
                           :date today
                           :todo "TODAY"
                           :scheduled today
                           :discard (:anything)
                           :order 1)))))
                       (alltodo "" ((org-agenda-overriding-header "CURRENT STATUS")
                                    (org-agenda-prefix-format "  %t  %s")
                          (org-super-agenda-groups
                           '((:log t)
                          ;   (:name "⚠⚠⚠ --- MISSION CRITICAL TASKS --- ⚠⚠⚠"
                          ;    :discard (:todo "DONE")
                          ;    :tag "CRITICAL"
                          ;    :order 2
                          ;    :transformer (--> it
                          ;         (upcase it)
                          ;         (propertize it 'face '(:foreground "red1"))))
                             (:name " 🚧🚧🚧 --- ACTIVE PROJECT(s) --- 🚧🚧🚧 "
                              :todo "PROJECT"
                              :order 6
                              :transformer (--> it
                                   (upcase it)
                                   (propertize it 'face '(:foreground "SlateBlue1"))))
                             (:name "〰️〰️〰 --- Currently Working On --- 〰〰〰"
                                    :todo "IN-PROGRESS"
                                    :order 4)
                             (:name "❗❗❗ --- Important --- ❗❗❗"
                                    :date today
                                    :discard (:todo "DONE")
                                    :priority "A"
                                    :order 10)
                             (:name "✅✅✅ --- GOAL --- ✅✅✅"
                                    :todo "GOAL"
                                    :order 2
                                    :transformer (--> it
                                         (upcase it)
                                         (propertize it 'face '(:foreground "LimeGreen"))))
                             (:name "✅✅✅ --- WEEKLY-GOALS --- ✅✅✅"
                                    :todo "WEEKLY-GOAL"
                                    :order 3
                                    :transformer (--> it
                                         (upcase it)
                                         (propertize it 'face '(:foreground "light sea green"))))
                             (:name "❌⚠❌ --- Overdue! --- ❌⚠❌"
                                    :discard (:todo "DONE")
                                    :deadline past
                                    :scheduled past
                                    :transformer (--> it
                                         (upcase it)
                                         (propertize it 'face '(:foreground "red")))
                                    :order 5)
                          ;   (:name " 🏃‍🏃‍‍🏃 --- Errands --- 🏃‍🏃‍‍🏃"
                          ;          :and (:scheduled today :tag "Errands")
                          ;          :order 12)
                          ;   (:name "⏲⏲⏲ --- Waiting --- ⏲⏲⏲"
                          ;          :todo "WAITING"
                          ;          :order 6)
                             (:name "🇧🇧🇧 --- WORK --- 🇧🇧🇧"
                                    :and (:tag "WORK" :todo "WORK")
                                    :order 9)
                          ;   (:name "📅 📅 📅 --- Calendar Events --- 📅 📅 📅"
                          ;          :and (:scheduled today :todo "CALENDAR")
                          ;          :order 19)
                          ;   (:name "✔✔✔ --- HABIT --- ✔✔✔"
                          ;          :and (:scheduled today :tag "habit")
                          ;          :transformer (--> it
                          ;               (upcase it)
                          ;               (propertize it 'face '(:foreground "royalblue1")))
                          ;          :order 20)
                            (:discard (:anything))))))))))
   :config
   (org-super-agenda-mode))

;; Journal Config
(setq org-journal-dir "/home/martin/Nextcloud/50-59_PersonalDevelopment/51-Diaries/51.01-Daily_Diaries"
      org-journal-date-prefix "#+TITLE: "
      org-journal-time-prefix "* "
      org-journal-date-format "%a, %d-%m-%Y"
      org-journal-file-format "%d-%m-%Y-jrnl.org")

;; Preview images in all org files on launch
(setq org-startup-with-inline-images t)
(beacon-mode 1)

; This still does not work (unsure why)
(add-hook 'dired-mode-hook 'org-download-enable)
(setq-default org-download-image-dir "/home/martin/Nextcloud/screenshots")

;; Enables auto tangling/exporting of code blocks to a unified code file form org mode.
;; It means I can jsut write code blocks in org with detailed documentation and this will export it all accordingly.
(use-package! org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default t))

;; Export using my custom smart quotes.
(setq org-export-with-smart-quotes t)

;; Hide emphasis markers in text this means that MD and org syntax icons will not show
;; effectively acts as preview.

(after! org
(setq org-hide-emphasis-markers t))

;;Customize ORG higlighting
;; this controls the color of bold, italic, underline, verbatim, strikethrough

(after! org
(setq org-emphasis-alist
  '(("*" (underline :weight black :foreground "#bd03f9" ))
    ("/" (:weight black :background "#f1fa8c" :foreground "#ff5555" ))
    ("_" (:weight black :foreground "#ff79c6" ))
    ("=" (:weight black :background "#f1fa8c" :foreground "#ff5555" ))
    ("~" (:background "DarkSeaGreen1" :foreground "dim gray" ))
    ("+" (bold :strike-through nil :foreground "#ff4500" )))))

;;Change how bullets look
(setq org-superstar-headline-bullets-list
  '(;; Original ones nicked from org-bullets
    ?◉
    ?○
    ?✸
    ?▷))

;; Custom bulleted list
(setq org-superstar-item-bullet-alist '((?* . ?⋆)
                                        (?+ . ?‣)
                                        (?- . ?•)))

;; Custom drop down icon.
(setq org-ellipsis " ⯯")

;Org GTD
;;(use-package! org-gtd :after org)
(after! org
(use-package! org-gtd
  :config
  (setq org-edna-use-inheritance t)
  (org-edna-mode)
  (map! :leader
        (:prefix ("d" . "org-gtd")
         :desc "Capture"        "c"  #'org-gtd-capture
         :desc "Engage"         "e"  #'org-gtd-engage
         :desc "Process inbox"  "i"  #'org-gtd-process-inbox
         :desc "Process item"   "p"  #'org-gtd-clarify-item
         :desc "Organize"       "o"  #'org-gtd-organize
         :desc "Archive"       "A"   #'org-gtd-archive-item-at-point
         :desc "Show all next"  "n"  #'org-gtd-show-all-next
         :desc "Set Area of Focus on item"  "a"  #'org-gtd-area-of-focus-set-on-item-at-point
         :desc "Review Area of Focus"  "r"  #'org-gtd-review-area-of-focus
         :desc "Refile Task"  "R"  #'org-refile
         :desc "Stuck projects" "s"  #'org-gtd-review-stuck-projects))
  (map! :map org-gtd-clarify-map
        :desc "Organize this item" "C-c c" #'org-gtd-organize)))

(setq org-gtd-directory "/home/martin/Nextcloud/01-09_System/01-Emacs/01.02-OrgGtd")

(setq org-gtd-areas-of-focus '("Home" "Health" "Career" "Finance" "Goal" "Systems" "Relationships" "Personal Development"))

;ORG - Start all documents in overview mode:
; I have large org files with lots of nested headings, this makes it less cumbersome.
(setq org-startup-folded t)

;ORG - Add ID to all ORG headindgs on save:
(add-hook 'org-capture-prepare-finalize-hook 'org-id-get-create)
(defun my/org-add-ids-to-headlines-in-file ()
;  "Add ID properties to all headlines in the current file which
;do not already have one."
  (interactive)
  (org-map-entries 'org-id-get-create))
(add-hook 'org-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'my/org-add-ids-to-headlines-in-file nil 'local)))

;Roam Main Dir
(setq org-roam-directory "~/Nextcloud")

; Roam - (make git ignored files in subdirectories still searchable)
(after! org-roam
  (setq org-roam-list-files-commands '(find fd fdfind rg)))

;Roam - Capture Templates:
(setq org-roam-capture-templates
'(("d" "default" plain
      "%?"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
  ("s" "Service" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/ServiceTemplate.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)
 ("t" "Tool" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/ToolTemplate.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)
 ("m" "Method" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/MethodTemplate.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)
 ("a" "Attack Type" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/AttackTemplate.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)
 ("c" "CPTS Module" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/CPTSSection.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)
 ("p" "Start Project" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/ProjectStartTemplate.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)
 ("P" "End Project" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/ProjectEndTemplate.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)
 ("b" "Box" plain
  (file "/home/martin/Nextcloud/01-09_System/04-Templates/BoxTemplate.org")
  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  :unnarrowed t)))

;; Open neotree with (SPC t n) open dir with (SPC d n)
(after! neotree
  (setq neo-smart-open t
        neo-window-fixed-size nil))
(after! doom-themes
  (setq doom-neotree-enable-variable-pitch t))
(map! :leader
      :desc "Toggle neotree file viewer" "t n" #'neotree-toggle
      :desc "Open directory in neotree" "d n" #'neotree-dir)

;; dired customizaion

(map! :leader
      (:prefix ("D" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "D p" #'peep-dired
        :desc "Dired view file" "D v" #'dired-view-file)))

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-do-chmod
  (kbd "O") 'dired-do-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-do-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "Z") 'dired-do-compress
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-do-kill-lines
  (kbd "% l") 'dired-downcase
  (kbd "% m") 'dired-mark-files-regexp
  (kbd "% u") 'dired-upcase
  (kbd "* %") 'dired-mark-files-regexp
  (kbd "* .") 'dired-mark-extension
  (kbd "* /") 'dired-mark-directories
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))

;Use VIM Keybindings to move between windows:
(define-key evil-motion-state-map (kbd "C-h") #'evil-window-left)
(define-key evil-motion-state-map (kbd "C-j") #'evil-window-down)
(define-key evil-motion-state-map (kbd "C-k") #'evil-window-up)
(define-key evil-motion-state-map (kbd "C-l") #'evil-window-right)

; Zoom in and Out easily
(defun my/increase-text-height ()
  (interactive)
  (text-scale-increase 1))

(defun my/decrease-text-height ()
  (interactive)
  (text-scale-decrease 1))

(global-set-key (kbd "C-=") 'my/increase-text-height)
(global-set-key (kbd "C--") 'my/decrease-text-height)

;; Enables Emofis
(use-package emojify
  :hook (after-init . global-emojify-mode))

;insert date shortcut
; Taken from dt/distro-tube.
(defun dt/insert-todays-date (prefix)
  (interactive "P")
  (let ((format (cond
                 ((not prefix) "%A, %B %d, %Y")
                 ((equal prefix '(4)) "%m-%d-%Y")
                 ((equal prefix '(16)) "%Y-%m-%d"))))
    (insert (format-time-string format))))

(require 'calendar)
(defun dt/insert-any-date (date)
  "Insert DATE using the current locale."
  (interactive (list (calendar-read-date)))
  (insert (calendar-date-string date)))

(map! :leader
      (:prefix ("i d" . "Insert date")
        :desc "Insert any date"    "a" #'dt/insert-any-date
        :desc "Insert todays date" "t" #'dt/insert-todays-date))

;Centaur Tabs:
(setq centaur-tabs-set-bar 'over
      centaur-tabs-set-icons t
      centaur-tabs-gray-out-icons 'buffer
      centaur-tabs-height 24
      centaur-tabs-set-modified-marker t
      centaur-tabs-style "bar"
      centaur-tabs-modified-marker "•")
(map! :leader
      :desc "Toggle tabs globally" "t c" #'centaur-tabs-mode
      :desc "Toggle tabs local display" "t C" #'centaur-tabs-local-mode)
(evil-define-key 'normal centaur-tabs-mode-map (kbd "g <right>") 'centaur-tabs-forward        ; default Doom binding is 'g t'

                                               (kbd "g <left>")  'centaur-tabs-backward       ; default Doom binding is 'g T'
                                               (kbd "g <down>")  'centaur-tabs-forward-group
                                               (kbd "g <up>")    'centaur-tabs-backward-group)

(beacon-mode 1)

;; Calendar
; https://stackoverflow.com/questions/9547912/emacs-calendar-show-more-than-3-months
(defun dt/year-calendar (&optional year)
  (interactive)
  (require 'calendar)
  (let* (
      (current-year (number-to-string (nth 5 (decode-time (current-time)))))
      (month 0)
      (year (if year year (string-to-number (format-time-string "%Y" (current-time))))))
    (switch-to-buffer (get-buffer-create calendar-buffer))
    (when (not (eq major-mode 'calendar-mode))
      (calendar-mode))
    (setq displayed-month month)
    (setq displayed-year year)
    (setq buffer-read-only nil)
    (erase-buffer)
    ;; horizontal rows
    (dotimes (j 4)
      ;; vertical columns
      (dotimes (i 3)
        (calendar-generate-month
          (setq month (+ month 1))
          year
          ;; indentation / spacing between months
          (+ 5 (* 25 i))))
      (goto-char (point-max))
      (insert (make-string (- 10 (count-lines (point-min) (point-max))) ?\n))
      (widen)
      (goto-char (point-max))
      (narrow-to-region (point-max) (point-max)))
    (widen)
    (goto-char (point-min))
    (setq buffer-read-only t)))

(defun dt/scroll-year-calendar-forward (&optional arg event)
  "Scroll the yearly calendar by year in a forward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (unless arg (setq arg 0))
  (save-selected-window
    (if (setq event (event-start event)) (select-window (posn-window event)))
    (unless (zerop arg)
      (let* (
              (year (+ displayed-year arg)))
        (dt/year-calendar year)))
    (goto-char (point-min))
    (run-hooks 'calendar-move-hook)))

(defun dt/scroll-year-calendar-backward (&optional arg event)
  "Scroll the yearly calendar by year in a backward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (dt/scroll-year-calendar-forward (- (or arg 1)) event))

(map! :leader
      :desc "Scroll year calendar backward" "<left>" #'dt/scroll-year-calendar-backward
      :desc "Scroll year calendar forward" "<right>" #'dt/scroll-year-calendar-forward)

(defalias 'year-calendar 'dt/year-calendar)

(use-package! calfw)
(use-package! calfw-org)

;; Markdown & line settings

(setq display-line-numbers-type t)
(map! :leader
      :desc "Comment or uncomment lines" "TAB TAB" #'comment-line
      (:prefix ("t" . "toggle")
       :desc "Toggle line numbers" "l" #'doom/toggle-line-numbers
       :desc "Toggle line highlight in frame" "h" #'hl-line-mode
       :desc "Toggle line highlight globally" "H" #'global-hl-line-mode
       :desc "Toggle truncate lines" "t" #'toggle-truncate-lines))

(custom-set-faces
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "variable-pitch"))))
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 1.7))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.6))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.5))))
 '(markdown-header-face-4 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-5 ((t (:inherit markdown-header-face :height 1.3))))
 '(markdown-header-face-6 ((t (:inherit markdown-header-face :height 1.2)))))

(defun dt/insert-todays-date (prefix)
  (interactive "P")
  (let ((format (cond
                 ((not prefix) "%A, %B %d, %Y")
                 ((equal prefix '(4)) "%m-%d-%Y")
                 ((equal prefix '(16)) "%Y-%m-%d"))))
    (insert (format-time-string format))))

(require 'calendar)
(defun dt/insert-any-date (date)
  "Insert DATE using the current locale."
  (interactive (list (calendar-read-date)))
  (insert (calendar-date-string date)))

(use-package! multi-vterm
  :after vterm)

;(use-package! golden-ratio)
;(golden-ratio-mode 1)
;(setq golden-ratio-auto-scale t)
